import 'dart:developer';

import '../../pod_player_new.dart';

void podLog(String message) =>
    PodVideoPlayer.enableLogs ? log(message, name: 'POD') : null;
